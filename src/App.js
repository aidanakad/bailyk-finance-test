import QuestionnaireView from "./view";

function App() {
  return (
    <div>
      <QuestionnaireView/>
    </div>
  );
}

export default App;
