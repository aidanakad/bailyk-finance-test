import { useEffect, useState } from 'react'
import { Card, Container} from "reactstrap";
import {getEmployees, getQuestions, postQuestions} from "../api";
import Question from "../components/question";
import CommentSection from "../modules/comments";
import {Button, Spinner} from "react-bootstrap";
import {useForm} from "react-hook-form";
import Form from "react-bootstrap/Form";
import MyModalAlert from "../components/toast";

const QuestionnaireView = () => {
    const [questions, setQuestions] = useState([])
    const [employees, setEmployees] = useState([])
    const [answers, setAnswers] = useState([])
    const [beLike, setBeLike] = useState(0)
    const [comment, setComment] = useState('')
    const [show, setShow] = useState(false)
    const [variant, setVariant] = useState('')
    const [isDisabled, setDisabled] = useState(true)
    const [isLoading, setLoading] = useState(false)

    const form = useForm()
    const { handleSubmit } = form

    useEffect(() => {
        setLoading(true)
        getQuestions()
            .then((res) =>
            setQuestions(res))
            .then(()=>setLoading(false))
        getEmployees().then((res)=>
        setEmployees(res)
        )
    }, [])

    useEffect(()=>{
        if (beLike > 0 && comment.length>0){
            setDisabled(false)
        }
    },[beLike, comment])

    const handleAnswerUpdate = (newAnswer) => {
        const existingAnswerIndex = answers.findIndex(a => a.question === newAnswer.question && a.employee === newAnswer.employee);
        if (existingAnswerIndex !== -1) {
            setAnswers([
                ...answers.slice(0, existingAnswerIndex),
                newAnswer,
                ...answers.slice(existingAnswerIndex + 1),
            ]);
        } else {
            setAnswers([...answers, newAnswer]);
        }
    };
    const toggleShowA = () => setShow(!show);
    const onSubmit = async () => {
        const form = {
            answers: answers,
            be_like: beLike,
            comment: comment
        }
        try{
            await postQuestions(form)
            toggleShowA()
            setVariant('success')
        }catch (e) {
            toggleShowA()
            setVariant('danger')
        }
    }

    return (
        <>
            <MyModalAlert show={show} setShow={toggleShowA} variant={variant} />
            <Container>
                <Card className="my-5 shadow-sm">

                    <Form  className="mx-3 p-3" onSubmit={handleSubmit(onSubmit)}>
                        <h3 className="text-center">Уважаемый коллега, просим Вас заполнить форму обратной связи сотрудников других отделов.</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <hr/>
                        {isLoading ?
                            <div className="text-center"><Spinner animation="border" /></div>
                            :
                            questions?.map((item, index)=>(
                            <Question
                                key={index}
                                employees={employees}
                                question={item}
                                onAnswerUpdate={handleAnswerUpdate}
                                index={index}/>
                        ))}
                        <hr/>
                        <CommentSection
                            employees={employees}
                            useForm={form}
                            setBeLike={setBeLike}
                            setComment={setComment}/>
                        <div className="text-right mt-5">
                            <Button type="submit" disabled={isDisabled}>Отправить</Button>
                        </div>
                    </Form>

                </Card>
            </Container>

        </>
    )
}

export default QuestionnaireView