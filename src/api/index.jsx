import axios from 'axios'

export const request = async (url, method, payload, formData, params) => {
    const api = 'https://beksdev.pythonanywhere.com/'

    try {
        const res = await axios({
            url: `${api}${url}`,
            headers: {
                ...(formData && { 'Content-Type': 'multipart/form-data' })
            },
            method,
            data: payload,
            params
        })
        return res.data
    } catch (error) {
        throw error
    }
}

export async function getQuestions() {
    return request(`questions/`, 'GET')
}

export async function postQuestions(payload) {
    return request(`questionnaires/`, 'POST', payload)
}

export async function getEmployees() {
    return request(`employees/`, 'GET')
}