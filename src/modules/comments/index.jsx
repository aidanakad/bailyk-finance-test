import Form from 'react-bootstrap/Form';
import {FloatingLabel} from "react-bootstrap";
const CommentSection = ({employees, setComment, setBeLike}) => {


    const handleChange = (id) =>{
        setBeLike(id * 1)
    }
    return(
        <>
            <div className="font-weight-bold mb-3">1. На кого из руководителей вы хотите написать комментарий</div>
            <Form.Select aria-label="Default select example"
                         onChange={(e)=>handleChange(e.target.value)}>
                <option>none</option>
                {employees.map((employee)=>(
                    <option
                        key={employee.id}
                        value={employee.id}
                    >
                        {employee.full_name}
                    </option>
                ))}
            </Form.Select>

            <div className="font-weight-bold my-3">2. Поделитесь, пожалуйста, своим мнением</div>
            <FloatingLabel controlId="floatingTextarea2" >
                <Form.Control
                    onChange={(e)=> setComment(e.target.value)}
                    as="textarea"
                    placeholder="Leave a comment here"
                    style={{ height: '100px' }}
                />
            </FloatingLabel>
        </>
    )
}

export default CommentSection