import {Form} from "react-bootstrap";
import {Col, Row} from "reactstrap";
const EmployeeOptions = ({employee, questionId,  onAnswerUpdate}) => {
    let options = [1,2,3,4,5]
    const handleChoose = ( score) =>{
        onAnswerUpdate({employee: employee.id, score: score, question: questionId})
    }

    return(
        <>
                <Row>
                    <Col sm={6} md={2} lg={2}>
                        <span>{employee.full_name}</span>
                    </Col>
                    <Col sm={6} md={10} lg={10}>
                        <div className="d-flex">
                            {options.map((option,index) => (
                                <div key={index} className="mb-3 ">
                                    <Form.Check
                                        onChange={() => handleChoose(option)}
                                        className="d-flex align-items-center"
                                        inline
                                        label={option}
                                        name={`group1-${employee.id}-${questionId}`}
                                        type="radio"
                                        id={`inline-radio-${option}`}
                                    />
                                </div>
                            ))}
                        </div>
                    </Col>
                </Row>
        </>
    )
}

export default EmployeeOptions