import {Button, Modal, Toast} from "react-bootstrap";

const MyModalAlert = ({ show, variant, setShow}) => {
    return(
        <>

            <Modal show={show} onHide={setShow} centered>
                <Modal.Header closeButton className={`bg-${variant}`}>
                    <Modal.Title >
                        <span className="text-white">Notification</span>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>{variant==='success' ? "Ваши ответы отправлены!" : "Что-то пошло не так, попробуйте еще раз"}</Modal.Body>
                <Modal.Footer>
                    <Button variant='secondary' onClick={()=> window.location.href = '/'}>Ok</Button>
                </Modal.Footer>
            </Modal>

        </>
    )
}

export default MyModalAlert