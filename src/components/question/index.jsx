import EmployeeOptions from "../employee-options";

const Question = ({employees, question, index,  onAnswerUpdate}) => {

    return(
        <>
            <div>
                <div className="font-weight-bold mb-3">
                    {index + 1}. {question?.value.charAt(0).toUpperCase() + question?.value.slice(1)}
                </div>
                {employees.map((employee)=>(
                    <EmployeeOptions employee={employee}
                                     key={employee.id}
                                     onAnswerUpdate={onAnswerUpdate}
                                     questionId={question?.id}/>
                ))}
            </div>

        </>
    )
}

export default Question